import React, { Component } from 'react';
import './App.css';
import Person from './Person/Person';

class App extends Component {

  state = {
    persons: [
      { name: "max", age:28 },
      { name: "manu", age:29 },
      { name: "stephanie", age:26 }
    ],
    otherState: 'Something here'
  }

  switchNameHandler = (newName) => {
    //DO NOT DO THIS: this.state.persons[0].state = "Maximilian";
    this.setState({
      persons: [
        { name: newName, age:28 },
        { name: "manu", age:29 },
        { name: "stephanie", age:31 }
    ]})
  }

  //new handler
  nameChangedHandler = (event) => {
    this.setState({
      persons: [
        { name: "max", age:28 },
        { name: event.target.value, age:29 },
        { name: "stephanie", age:26 }
    ]})
  }

  render() {

    const style = {
      backgroundColor: 'white',
      font: 'inherit',
      border: '1px solid blue',
      padding: '8px',
      cursor: 'pointer'
    };

    return (
      <div className="App">
        <h1>Hi, I'm a React app</h1>
        <p>This is a paragraph</p>
          <button 
          style={style}
          onClick={() => this.switchNameHandler('Alexanro')}>Switch name</button>
        <Person 
          name={this.state.persons[0].name} 
          age={this.state.persons[0].age}/>
        <Person 
          name={this.state.persons[1].name} 
          age={this.state.persons[1].age}
          click={this.switchNameHandler.bind(this, "Max!!")}
          changed={this.nameChangedHandler}>My hobbie photography</Person>
        <Person 
          name={this.state.persons[2].name} 
          age={this.state.persons[2].age}/>
      </div>
    );
  }
}

export default App;
